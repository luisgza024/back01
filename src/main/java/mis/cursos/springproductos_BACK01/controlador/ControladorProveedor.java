package mis.cursos.springproductos_BACK01.controlador;

import mis.cursos.springproductos_BACK01.modelo.Proveedor;
import mis.cursos.springproductos_BACK01.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/almacen/v1/proveedores")
@ExposesResourceFor(Proveedor.class)
public class ControladorProveedor {
    @Autowired
    ServicioGenerico<Proveedor> servicioProveedor; // Inyección de Dependencias

    @Autowired
    EntityLinks entityLinks;

    @GetMapping
    public CollectionModel<EntityModel<Proveedor>> obtenerProveedores() {
        final Map<Long, Proveedor> mapPro = this.servicioProveedor.obtenerTodos();
        List<Proveedor> prov = mapPro.values().stream().collect(Collectors.toList());
        return CollectionModel.of(
                prov.stream().map(p -> obtenerRespuestaProveedor(p)).collect(Collectors.toUnmodifiableList())
        ).add(linkTo(methodOn(this.getClass()).obtenerProveedores()).withSelfRel()
        );
    }

    @GetMapping("/{idProveedor}")
    public EntityModel<Proveedor> obtenerProveedorPorId(@PathVariable(name = "idProveedor") long id) {
        try {
            final Proveedor p = this.servicioProveedor.obtenerPorId(id);
            return obtenerRespuestaProveedor(p);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping
    public ResponseEntity<EntityModel<Proveedor>> crearProveedor(@RequestBody Proveedor p) {
        final long id = this.servicioProveedor.agregar(p);
        p.setId(id);
        return ResponseEntity
                .ok()
                .location(crearEnlaceProveedor(p).toUri())
                .body(obtenerRespuestaProveedor(p))
                ;
    }

    @PutMapping("/{idProveedor}")
    public void reemplazarProveedorPorId(@PathVariable(name = "idProveedor") long id,
                                         @RequestBody Proveedor p) {
        try {
            p.setId(id);
            this.servicioProveedor.reemplazarPorId(id, p);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{idProveedor}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarProveedorPorId(@PathVariable(name = "idProveedor") long id) {
        this.servicioProveedor.borrarPorId(id);
    }

    private Link crearEnlaceProveedor(Proveedor p) {
        return this.entityLinks
                .linkToItemResource(p.getClass(), p.getId())
                .withSelfRel()
                .withTitle("Detalles de este proveedor");
    }

    // private List<Link> crearEnlacesAdicionalesProveedor(Proveedor p) {
    //    return Arrays.asList(
    //           crearEnlaceProveedor(p),
    //         linkTo(methodOn(this.getClass()).obtenerProveedores()).withRel("proveedores").withTitle("Todos los proveedores")
    //);
    //}

    private EntityModel<Proveedor> obtenerRespuestaProveedor(Proveedor p) {
        return EntityModel.of(p).add(crearEnlaceProveedor(p));
    }
}
