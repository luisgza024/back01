package mis.cursos.springproductos_BACK01.controlador;

import mis.cursos.springproductos_BACK01.modelo.Usuario;
import mis.cursos.springproductos_BACK01.servicio.ServicioGenerico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.EntityLinks;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
@RestController
@RequestMapping("/almacen/v1/usuarios")
@ExposesResourceFor(Usuario.class)
public class ControladorUsuario {

    @Autowired
    ServicioGenerico<Usuario> servicioUsuario; // Inyección de Dependencias

    @Autowired
    EntityLinks entityLinks;

    @GetMapping
    public CollectionModel<EntityModel<Usuario>> obtenerUsuarios() {
        final Map<Long, Usuario> mp = this.servicioUsuario.obtenerTodos();
        List<Usuario> listaUsuarios = mp.values().stream().collect(Collectors.toList());
        return CollectionModel.of(
                listaUsuarios.stream().map(u -> obtenerRespuestaUsuario(u)).collect(Collectors.toUnmodifiableList())
        ).add(linkTo(methodOn(this.getClass()).obtenerUsuarios()).withSelfRel().withTitle("Lista de usuarios"));
    }

    @GetMapping("/{idUsuario}")
    public EntityModel<Usuario> obtenerUsuarioPorId(@PathVariable(name = "idUsuario") long id) {
        Usuario u = this.servicioUsuario.obtenerPorId(id);
        if (u != null)
            return obtenerRespuestaUsuario(u);
        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    private Link crearEnlaceUsuario(Usuario u) {
        return this.entityLinks
                .linkToItemResource(u.getClass(), u.getId())
                .withSelfRel()
                .withTitle("Detalles del usuario");
    }

    private EntityModel<Usuario> obtenerRespuestaUsuario(Usuario u) {
        return EntityModel.of(u).add(crearEnlaceUsuario(u));
    }

    @DeleteMapping("/{idUsuario}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarUsuarioPorId(@PathVariable(name = "idUsuario") long id) {
        this.servicioUsuario.borrarPorId(id);
    }

    static class UsuarioAux {
        public String nombre;
    }

    @PatchMapping("/{idUsuario}")
    public void parcharUsuarioPorId(@PathVariable(name = "idUsuario") long id,
                                    @RequestBody UsuarioAux userAux) {
        try {
            Usuario u = this.servicioUsuario.obtenerPorId(id);
            if (userAux.nombre != null || userAux.nombre.isBlank())
                u.setNombre(userAux.nombre);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{idUsuario}")
    public void reemplazarUsuarioPorId(@PathVariable(name = "idUsuario") long id,
                                       @RequestBody Usuario u) {
        try {
            u.setId(id);
            this.servicioUsuario.reemplazarPorId(id, u);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<EntityModel<Usuario>> crearProducto(@RequestBody Usuario u) {
        long id = this.servicioUsuario.agregar(u);
        u.setId(id);
        return ResponseEntity
                .ok()
                .location(crearEnlaceUsuario(u).toUri())
                .body(obtenerRespuestaUsuario(u))
                ;
    }
}