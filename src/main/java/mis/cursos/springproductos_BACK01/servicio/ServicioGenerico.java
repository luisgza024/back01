package mis.cursos.springproductos_BACK01.servicio;

import java.util.Map;

public interface ServicioGenerico<T> {

    // CREATE
    public long agregar(T t);

    // READ []
    public Map<Long, T> obtenerTodos();

    // READ
    public T obtenerPorId(long id);

    // UPDATE
    public void reemplazarPorId(long id, T t);

    // DELETE
    public void borrarPorId(long id);

}
