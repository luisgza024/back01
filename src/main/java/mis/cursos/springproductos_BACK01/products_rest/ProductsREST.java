package mis.cursos.springproductos_BACK01.products_rest;

import mis.cursos.springproductos_BACK01.entitys.Product;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("products")
public class ProductsREST {

    @GetMapping
    public ResponseEntity<Product> getProduct() {
        Product product = new Product();
        product.setId(1L);
        product.setName("Producto 1");
        return ResponseEntity.ok(product);
    }


    //@GetMapping
    //public ResponseEntity getProductId(@PathVariable int id) {
    //Product product = product;
    //product.setId(1L);
    //product.setName("Producto 1");
    //return ResponseEntity.ok(product);
    //}

    //public String Product(@RequestParam(value = "name", defaultValue = "Laptop") String name){
    //    return String.format("Tu producto es: %s", name);
    //}



    //@GetMapping
    //@RequestMapping(value = "hello", method = RequestMethod.GET)
    //public String hell(){
//    return "Hello World";
//}
}
